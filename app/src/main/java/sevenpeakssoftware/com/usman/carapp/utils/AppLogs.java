package sevenpeakssoftware.com.usman.carapp.utils;

import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Calendar;

/*
 * this class will be centralised for all application logs collection
 * for analytics and crash / errors states
 */

public class AppLogs {

    public static boolean DEBUG_APP = true;
    public static boolean CREATE_LOG_FILE = false;
    //
    public static boolean ALLOW_SENTRY_DEBUG = false;
    public static boolean ALLOW_SENTRY_ERROR = false;

    // public static boolean ALLOW_SENTRY_WARNING = false;
    // public static boolean ALLOW_SENTRY_INFO = false;

    public static void error(String tag, Exception e) {
        if (DEBUG_APP) {
            e.printStackTrace();
            Log.e(tag, e.toString());
        }
        if (CREATE_LOG_FILE)
            createAppendLog(tag, e);
        /*if (ALLOW_SENTRY_ERROR)
            sentryCaptureException(e, Sentry.SentryEventBuilder.SentryEventLevel.ERROR);*/
    }

    public static void error(String tag, String text) {
        if (DEBUG_APP)
            Log.e(tag, text);
        /*if (ALLOW_SENTRY_ERROR)
            sentryCaptureMessage("TAG : " + tag + "    MESSAGE : " + text, Sentry.SentryEventBuilder.SentryEventLevel.ERROR);*/
    }

    public static void debug(String tag, Exception e) {
        if (DEBUG_APP) {
            e.printStackTrace();
            Log.d(tag, e.toString());
        }
        if (CREATE_LOG_FILE)
            createAppendLog(tag, e);
        /*if (ALLOW_SENTRY_DEBUG)
            sentryCaptureException(e, Sentry.SentryEventBuilder.SentryEventLevel.DEBUG);*/
    }

    public static void debug(String tag, String text) {
        if (DEBUG_APP)
            Log.d(tag, text);
        /*if (ALLOW_SENTRY_DEBUG)
            sentryCaptureMessage("TAG : " + tag + "    MESSAGE : " + text, Sentry.SentryEventBuilder.SentryEventLevel.DEBUG);*/
    }

    public static void info(String tag, Exception e) {
        if (DEBUG_APP) {
            e.printStackTrace();
            Log.i(tag, e.toString());
        }
        if (CREATE_LOG_FILE)
            createAppendLog(tag, e);
        /*if (ALLOW_SENTRY_INFO)
            sentryCaptureException(e, Sentry.SentryEventBuilder.SentryEventLevel.INFO);*/
    }

    public static void info(String tag, String text) {
        if (DEBUG_APP)
            Log.i(tag, text);
        /*if (ALLOW_SENTRY_INFO)
            sentryCaptureMessage("TAG : " + tag + "    MESSAGE : " + text, Sentry.SentryEventBuilder.SentryEventLevel.INFO);*/
    }

    public static void warn(String tag, Exception e) {
        if (DEBUG_APP) {
            e.printStackTrace();
            Log.w(tag, e.toString());
        }
        if (CREATE_LOG_FILE)
            createAppendLog(tag, e);
        /*if (ALLOW_SENTRY_WARNING)
            sentryCaptureException(e, Sentry.SentryEventBuilder.SentryEventLevel.WARNING);*/
    }

    public static void warn(String tag, String text) {
        if (DEBUG_APP)
            Log.w(tag, text);
        /*if (ALLOW_SENTRY_WARNING)
            sentryCaptureMessage("TAG : " + tag + "    MESSAGE : " + text, Sentry.SentryEventBuilder.SentryEventLevel.WARNING);*/
    }

    public static void verbose(String tag, Exception e) {
        if (DEBUG_APP) {
            e.printStackTrace();
            Log.v(tag, e.toString());
        }
        if (CREATE_LOG_FILE)
            createAppendLog(tag, e);
        /*if (ALLOW_SENTRY_WARNING)
            sentryCaptureException(e, Sentry.SentryEventBuilder.SentryEventLevel.WARNING);*/
    }

    public static void verbose(String tag, String text) {
        if (DEBUG_APP)
            Log.v(tag, text);
        /*if (ALLOW_SENTRY_WARNING)
            sentryCaptureMessage("TAG : " + tag + "    MESSAGE : " + text, Sentry.SentryEventBuilder.SentryEventLevel.WARNING);*/
    }

    public static void caughtException(Throwable cause) {
        cause.printStackTrace();
    }

    private static void createAppendLog(String tag, Exception e) {
        File logFolder = new File(AppConstants.APP_FOLDER_BASE_PATH + AppConstants.LOG_NAME);
        if (!logFolder.exists())
            Log.d("FURSAN_LOG", logFolder.mkdirs() ? AppConstants.LOG_NAME + " is created" :
                    AppConstants.LOG_NAME + " couldn't created");
        Calendar calendar = DateUtils.getCalendar();
        String logFileName = "FURSAN_LOG-" + DateUtils.convertDate(calendar.getTimeInMillis(), DateUtils.SERVER_DATE_TIMESTAMP) + ".txt";
        File logFile = new File(logFolder + File.separator + logFileName);
        try {
            StringWriter result = new StringWriter();
            PrintWriter printWriter = new PrintWriter(result);
            printWriter.append("\n");
            printWriter.append("/***      Log (").append(tag).append(") Added at ").
                    append(calendar.getTime().toString()).append("      ***/ \n");
            e.printStackTrace(printWriter);
            ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(logFile));
            oos.writeObject(e);
            oos.flush();
            oos.close();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
    }
}
