package sevenpeakssoftware.com.usman.carapp.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class DateUtils {

    // date formats
    public static final String SERVER_DATE_TIMESTAMP = "dd.mm.yyyy HH:mm";
    public static final String SERVER_DATE = "dd.mm.yyyy";
    public static final String DATE_FORMAT_MONTH_DAY = "dd MMMM";
    public static final String DATE_FORMAT_MONTH_DAY_YEAR = "dd MMMM yyyy";

    public static final String TIME_FORMAT_24 = "HH:mm";
    public static final String TIME_FORMAT_12 = "HH:mm aa";

    public static final String HOUR_FORMAT_1 = "h:mm a";
    public static final String CALENDAR_DEFAULT_FORMAT = "EE MMM dd HH:mm:ss z yyyy";

    private DateUtils() {
        // This utility class is not publicly instantiable
    }

    public static Calendar getCalendar() {
        Calendar calendar = Calendar.getInstance(Locale.US);
        calendar.setFirstDayOfWeek(Calendar.SUNDAY);
        return calendar;
    }

    public static String getDateTimeStampText(String dateTimeFromServer) {
        int currentYear = Calendar.getInstance().get(Calendar.YEAR);

        if (dateTimeFromServer.contains(currentYear + ""))
            return oneFormatToAnother(dateTimeFromServer, SERVER_DATE, DATE_FORMAT_MONTH_DAY);
        else
            return oneFormatToAnother(dateTimeFromServer, SERVER_DATE, DATE_FORMAT_MONTH_DAY_YEAR);
    }

    public static String oneFormatToAnother(String date, String oldFormat, String newFormat) {
        try {
            SimpleDateFormat originalFormat = new SimpleDateFormat(oldFormat,
                    Locale.ENGLISH);
            SimpleDateFormat targetFormat = new SimpleDateFormat(newFormat,
                    Locale.ENGLISH);
            Date d = originalFormat.parse(date);
            return targetFormat.format(d);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String convert24To12HourTime(String timeStringToConvert) {
        String formattedTime = "";
        SimpleDateFormat readFormat = new SimpleDateFormat(TIME_FORMAT_24, Locale.ENGLISH);
        SimpleDateFormat writeFormat = new SimpleDateFormat(TIME_FORMAT_12, Locale.ENGLISH);
        Date date = null;
        try {
            date = readFormat.parse(timeStringToConvert);
            formattedTime = writeFormat.format(date);
        } catch (java.text.ParseException e) {
            e.printStackTrace();
        }
        return formattedTime;
    }

    public static Date stringToDate(String date, String format) {
        try {
            SimpleDateFormat originalFormat = new SimpleDateFormat(format, Locale.ENGLISH);
            return originalFormat.parse(date);
        } catch (Exception e) {
            return null;
        }
    }

    public static Calendar dateToCalendar(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal;
    }

    static String convertDate(long dateInMilliseconds, String dateFormat) {
        return (new SimpleDateFormat(dateFormat, Locale.getDefault()).format(new Date(dateInMilliseconds)));
    }

}
