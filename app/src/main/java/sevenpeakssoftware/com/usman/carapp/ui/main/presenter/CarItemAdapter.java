package sevenpeakssoftware.com.usman.carapp.ui.main.presenter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import sevenpeakssoftware.com.usman.carapp.R;
import sevenpeakssoftware.com.usman.carapp.data.model.ContentItem;
import sevenpeakssoftware.com.usman.carapp.utils.DateUtils;

public class CarItemAdapter extends RecyclerView.Adapter<CarItemAdapter.CarItemViewHolder> {
    private List<ContentItem> carRepoList;
    private int position;
    private Context mContext;

    public CarItemAdapter(Context context) {
        mContext = context;
    }

    @NonNull
    @Override
    public CarItemAdapter.CarItemViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.car_repo_list_item, null);
        return new CarItemAdapter.CarItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CarItemViewHolder carItemViewHolder, int i) {
        animateItems(carItemViewHolder);
        ContentItem carContentItem = carRepoList.get(i);
        // apply row creation logic
        carItemViewHolder.tvTitle.setText(carContentItem.getTitle());
        Picasso.with(mContext).load(carContentItem.getImage()).into(carItemViewHolder.itemImage);
        carItemViewHolder.tvDescription.setText(carContentItem.getIngress());

        // apply date time formatting here
        String[] dateTime = carContentItem.getDateTime().split(" ");
        dateTime[0] = DateUtils.getDateTimeStampText(dateTime[0]);
        boolean is24HourSystem = android.text.format.DateFormat.is24HourFormat(mContext);
        if (!is24HourSystem)
            dateTime[1] = DateUtils.convert24To12HourTime(dateTime[1]);
        carItemViewHolder.tvDateTime.setText(String.format("%s %s", dateTime[0], dateTime[1]));
    }

    @Override
    public int getItemCount() {
        return (null != carRepoList ? carRepoList.size() : 0);
    }

    public void addAll(List<ContentItem> carRepoList) {
        this.carRepoList = carRepoList;
    }

    ContentItem getCurrentClickedItem(View view) {
        CarItemAdapter.CarItemViewHolder holder = (CarItemAdapter.CarItemViewHolder) view.getTag();
        return carRepoList.get(holder.getAdapterPosition());
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    private void animateItems(@NonNull CarItemViewHolder carItemViewHolder) {
        Animation animation = AnimationUtils.loadAnimation(mContext, R.anim.rv_slide_from_bottom);
        if (animation != null)
            carItemViewHolder.itemView.startAnimation(animation);
    }

    static class CarItemViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.itemImage)
        ImageView itemImage;
        @BindView(R.id.tvTitle)
        TextView tvTitle;
        @BindView(R.id.tvDescription)
        TextView tvDescription;
        @BindView(R.id.tvDateTime)
        TextView tvDateTime;

        CarItemViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

}
