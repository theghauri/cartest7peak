package sevenpeakssoftware.com.usman.carapp.data.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import co.uk.rushorm.core.RushObject;
import co.uk.rushorm.core.annotations.RushList;

public class ContentItem extends RushObject {

    @SerializedName("dateTime")
    private String dateTime;

    @SerializedName("ingress")
    private String ingress;

    @SerializedName("image")
    private String image;

    @SerializedName("created")
    private int created;

    @SerializedName("id")
    private int contentId;

    @SerializedName("title")
    private String title;

    @RushList(classType = ContentItem.class)
    @SerializedName("content")
    private List<ContentItem> content;

    @SerializedName("tags")
    private List<Object> tags;

    @SerializedName("changed")
    private int changed;

    // Content inside content
    @SerializedName("subject")
    private String subject;

    @SerializedName("description")
    private String description;

    @SerializedName("type")
    private String type;
    //

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public String getIngress() {
        return ingress;
    }

    public void setIngress(String ingress) {
        this.ingress = ingress;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getCreated() {
        return created;
    }

    public void setCreated(int created) {
        this.created = created;
    }

    public int getContentId() {
        return contentId;
    }

    public void setContentId(int contentId) {
        this.contentId = contentId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<ContentItem> getContent() {
        return content;
    }

    public void setContent(List<ContentItem> content) {
        this.content = content;
    }

    public List<Object> getTags() {
        return tags;
    }

    public void setTags(List<Object> tags) {
        this.tags = tags;
    }

    public int getChanged() {
        return changed;
    }

    public void setChanged(int changed) {
        this.changed = changed;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

}