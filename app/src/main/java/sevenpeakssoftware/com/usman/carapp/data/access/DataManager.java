package sevenpeakssoftware.com.usman.carapp.data.access;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;

import co.uk.rushorm.android.AndroidInitializeConfig;
import co.uk.rushorm.core.Rush;
import co.uk.rushorm.core.RushCore;
import co.uk.rushorm.core.RushSearch;
import sevenpeakssoftware.com.usman.carapp.data.model.CarListModel;
import sevenpeakssoftware.com.usman.carapp.data.model.ContentItem;


///*
//  This class will be responsible for saving data to local persistent, either prefrence or db
//*/

public class DataManager {

    ///*
    //  initialize DB with application context and all the rush objects
    //*/

    // region DB_INIT
    public void initDatabase(Context context) {
        AndroidInitializeConfig config = new AndroidInitializeConfig(context, getRushClasses());
        RushCore.initialize(config);
    }

    private List<Class<? extends Rush>> getRushClasses() {
        List<Class<? extends Rush>> classes = new ArrayList<>();
        classes.add(CarListModel.class);
        classes.add(ContentItem.class);
        return classes;
    }
    // endregion

    ///*
    //  CarTable getter and setter for DB
    //*/

    // region DB_CAR_GETTER_SETTER
    public void saveDataToLocal(CarListModel carListModel) {
        carListModel.save();
    }

    public List<ContentItem> getDataFromLocal() {
        List<CarListModel> carListDataModel = new RushSearch().find(CarListModel.class);
        return carListDataModel.get(0) != null && carListDataModel.get(0).getContent() != null ? carListDataModel.get(0).getContent() : null;
    }
    // endregion

}
