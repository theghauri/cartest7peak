package sevenpeakssoftware.com.usman.carapp.data.remote;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.interfaces.JSONObjectRequestListener;

import javax.inject.Inject;
import javax.inject.Singleton;


///*
//  Helper Class for all network calls
//*/

@Singleton
public class ApiHelper {

    // Urls and variables
    private static final String BASE_URL = "https://www.apphusetreach.no/application/119267";
    private static final String GET_CAR_DATA = BASE_URL + "/article/get_articles_list";


    @Inject
    public ApiHelper() {

    }

    //Car Data calling network api
    public void getCarData(JSONObjectRequestListener listener) {
        AndroidNetworking.get(GET_CAR_DATA)
                .build()
                .getAsJSONObject(listener);
    }
}
