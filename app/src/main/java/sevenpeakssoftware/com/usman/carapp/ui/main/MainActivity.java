package sevenpeakssoftware.com.usman.carapp.ui.main;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import co.uk.rushorm.core.RushSearch;
import sevenpeakssoftware.com.usman.carapp.R;
import sevenpeakssoftware.com.usman.carapp.data.access.DataManager;
import sevenpeakssoftware.com.usman.carapp.data.model.CarListModel;
import sevenpeakssoftware.com.usman.carapp.data.model.ContentItem;
import sevenpeakssoftware.com.usman.carapp.data.remote.ApiHelper;
import sevenpeakssoftware.com.usman.carapp.ui.main.presenter.CarItemAdapter;
import sevenpeakssoftware.com.usman.carapp.utils.AppLogs;
import sevenpeakssoftware.com.usman.carapp.utils.NetworkUtils;

public class MainActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {

    // region VIEW-INJECTION
    @BindView(R.id.rvCarRow)
    RecyclerView rvCarRow;
    @BindView(R.id.emptyLayout)
    LinearLayout emptyLayout;
    @BindView(R.id.ivEmptyResult)
    ImageView ivEmptyResult;
    @BindView(R.id.tvEmptyResult)
    TextView tvEmptyResult;
    @BindView(R.id.swipe_layout)
    SwipeRefreshLayout swipeRefresh;
    // endregion

    // region Local-Variables
    CarItemAdapter carItemAdapter;
    // endregion

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setContent();
    }

    private void setContent() {
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        rvCarRow.setLayoutManager(mLayoutManager);
        rvCarRow.setHasFixedSize(true);
        swipeRefresh.setOnRefreshListener(this);
        carItemAdapter = new CarItemAdapter(MainActivity.this);
        /* Get all objects */
        List<CarListModel> objects = new RushSearch().find(CarListModel.class);
        if (objects.size() == 0)
            fetchListFromWeb();
        else
            setAdapterFromList(objects.get(0).getContent());
    }

    @Override
    public void onRefresh() {
        fetchListFromWeb();
    }

    private void fetchListFromWeb() {
        if (NetworkUtils.isNetworkConnected(this)) {
            swipeRefresh.setRefreshing(true);
            new ApiHelper().getCarData(new JSONObjectRequestListener() {
                @Override
                public void onResponse(JSONObject response) {
                    swipeRefresh.setRefreshing(false);
                    setAdapterFromNetworkResponse(response.toString());
                }

                @Override
                public void onError(ANError anError) {
                    swipeRefresh.setRefreshing(false);
                    AppLogs.error("anError" + anError.getErrorCode(), anError.toString());
                    setErrorState(anError);
                }
            });
        } else {
            if (carItemAdapter == null || carItemAdapter.getItemCount() == 0)
                showNoNetworkState();
            else {
                swipeRefresh.setRefreshing(false);
                Snackbar.make(findViewById(R.id.activity_main),
                        R.string.no_connection, Snackbar.LENGTH_SHORT).show();
            }
        }
    }

    private void setAdapterFromNetworkResponse(String response) {
        CarListModel carListModel = new Gson().fromJson(response, CarListModel.class);
        DataManager dataManager = new DataManager();
        if (carListModel.getContent() != null && carListModel.getContent().size() != 0) {
            dataManager.saveDataToLocal(carListModel);
            emptyLayout.setVisibility(View.GONE);
            setAdapterFromList(carListModel.getContent());
        } else {
            showErrorState();
        }
    }

    private void setAdapterFromList(List<ContentItem> carRepoList) {
        carItemAdapter.addAll(carRepoList);
        if (rvCarRow.getAdapter() == null)
            rvCarRow.setAdapter(carItemAdapter);
        else
            carItemAdapter.notifyDataSetChanged();
    }

    private void setErrorState(ANError anError) {
        // Error Handling in case there is some sort of error from api
        // fetch data from db and populate on screen otherwise show appropriate error screen
        if (anError.getErrorCode() == 0) {
            if (carItemAdapter == null || carItemAdapter.getItemCount() == 0) {
                showErrorState();
            } else {
                DataManager dataManager = new DataManager();
                if (dataManager.getDataFromLocal() != null)
                    setAdapterFromList(dataManager.getDataFromLocal());
                else
                    showErrorState();
            }
        } else {
            showNoNetworkState();
        }
    }

    // Set screen to error state
    private void showErrorState() {
        emptyLayout.setVisibility(View.VISIBLE);
        ivEmptyResult.setImageDrawable(getResources().getDrawable(R.drawable.ic_empty_list));
        tvEmptyResult.setText(R.string.empty_result);

        Snackbar internetSnack = Snackbar.make(findViewById(R.id.activity_main),
                R.string.exception_message, Snackbar.LENGTH_INDEFINITE);
        internetSnack.setAction(R.string.retry_cta, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fetchListFromWeb();
            }
        });
        internetSnack.show();
    }

    // Set screen to no network state
    private void showNoNetworkState() {
        emptyLayout.setVisibility(View.VISIBLE);
        ivEmptyResult.setImageDrawable(getResources().getDrawable(R.drawable.ic_no_net));
        tvEmptyResult.setText(R.string.no_connection_main);

        Snackbar internetSnack = Snackbar.make(findViewById(R.id.activity_main),
                R.string.no_connection, Snackbar.LENGTH_INDEFINITE);
        internetSnack.setAction(R.string.retry_cta, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fetchListFromWeb();
            }
        });
        internetSnack.show();
    }
}
