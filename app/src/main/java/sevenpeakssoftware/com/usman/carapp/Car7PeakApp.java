package sevenpeakssoftware.com.usman.carapp;

import android.app.Application;
import android.content.Context;

import com.androidnetworking.AndroidNetworking;

import sevenpeakssoftware.com.usman.carapp.data.access.DataManager;

public class Car7PeakApp extends Application {

    private static Context applicationContext;

    public static Context getContext() {
        return applicationContext;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        applicationContext = getApplicationContext();
        // Initialize Network Helper
        AndroidNetworking.initialize(applicationContext);
        // Initialize DataBase Manager
        DataManager dataManager = new DataManager();
        dataManager.initDatabase(applicationContext);
    }


}
