package sevenpeakssoftware.com.usman.carapp.data.listeners;

///*
//  Interface for network call listener
//*/

public interface NetworkDataListener {

    void onResponse(String data);

    void onError(String error);

}
