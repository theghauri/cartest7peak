package sevenpeakssoftware.com.usman.carapp.utils;

import sevenpeakssoftware.com.usman.carapp.data.access.PreferenceManager;

import static android.os.Environment.getExternalStorageDirectory;
import static java.io.File.separator;

public class AppConstants {

    static final String LOG_NAME = "logs";

    static final String APP_FOLDER_BASE_PATH = getExternalStorageDirectory() + separator + PreferenceManager.APP_PREFERENCE_NAME + separator;
    //Regular Expressions
    public static String passwordPattern = "^(?=.*[A-Za-z])(?=.*\\d)(?=.*[$.@$!%*#?&])[A-Za-z\\d$.@$!%*#?&]{8,}$";
    static String namePattern = "^[a-zA-Z. ](\\s?[a-zA-Z. ]){2,}$";

}
