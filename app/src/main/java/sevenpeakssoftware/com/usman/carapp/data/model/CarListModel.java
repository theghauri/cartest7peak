package sevenpeakssoftware.com.usman.carapp.data.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import co.uk.rushorm.core.RushObject;
import co.uk.rushorm.core.annotations.RushList;

public class CarListModel extends RushObject {

    @SerializedName("serverTime")
    private int serverTime;

    @RushList(classType = ContentItem.class)
    @SerializedName("content")
    private List<ContentItem> content;

    @SerializedName("status")
    private String status;

    public int getServerTime() {
        return serverTime;
    }

    public void setServerTime(int serverTime) {
        this.serverTime = serverTime;
    }

    public List<ContentItem> getContent() {
        return content;
    }

    public void setContent(List<ContentItem> content) {
        this.content = content;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}