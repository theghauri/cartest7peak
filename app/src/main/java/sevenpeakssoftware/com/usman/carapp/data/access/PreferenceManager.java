package sevenpeakssoftware.com.usman.carapp.data.access;

import android.content.Context;
import android.content.SharedPreferences;

import sevenpeakssoftware.com.usman.carapp.Car7PeakApp;

///*
//  This class will be responsible for reading and writing to App private prefrence
//*/

public class PreferenceManager {

    // region PREFERENCE-KEYS
    public static final String BASE_URL_KEY = "base_url";
    private static final int PRIVATE_MODE = 0;
    public static final String APP_PREFERENCE_NAME = "7PeakSoftware";
    private static PreferenceManager sessionManager = null;
    private SharedPreferences mPreferences;
    private SharedPreferences.Editor mEditor;
    // endregion

    private PreferenceManager(Context context) {
        mPreferences = context.getSharedPreferences(APP_PREFERENCE_NAME, PRIVATE_MODE);
        mEditor = mPreferences.edit();
        mEditor.apply();
    }

    public static PreferenceManager getInstance() {
        if (sessionManager == null)
            sessionManager = new PreferenceManager(Car7PeakApp.getContext());
        return sessionManager;
    }

    public SharedPreferences getSharedPreferences() {
        return mPreferences;
    }

    public void clearPrefsData() {
        mEditor.clear();
        mEditor.commit();
    }

    public String read(String valueKey, String valueDefault) {
        return mPreferences.getString(valueKey, valueDefault);
    }

    public void save(String valueKey, String value) {
        mEditor.putString(valueKey, value);
        mEditor.commit();
    }

    public int read(String valueKey, int valueDefault) {
        return mPreferences.getInt(valueKey, valueDefault);
    }

    public void save(String valueKey, int value) {
        mEditor.putInt(valueKey, value);
        mEditor.commit();
    }

    public boolean read(String valueKey, boolean valueDefault) {
        return mPreferences.getBoolean(valueKey, valueDefault);
    }

    public void save(String valueKey, boolean value) {
        mEditor.putBoolean(valueKey, value);
        mEditor.commit();
    }

    public long read(String valueKey, long valueDefault) {
        return mPreferences.getLong(valueKey, valueDefault);
    }

    public void save(String valueKey, long value) {
        mEditor.putLong(valueKey, value);
        mEditor.commit();
    }

    public void saveID(String valueKey, long value) {
        mEditor.putLong(valueKey, value);
        mEditor.commit();
    }

    public long readID(String valueKey, long valueDefault) {
        return mPreferences.getLong(valueKey, valueDefault);
    }

    public float read(String valueKey, float valueDefault) {
        return mPreferences.getFloat(valueKey, valueDefault);
    }

    public void save(String valueKey, float value) {
        mEditor.putFloat(valueKey, value);
        mEditor.commit();
    }

}
